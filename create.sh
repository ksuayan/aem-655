#!/bin/bash

mvn -B archetype:generate \
 -D archetypeGroupId=com.adobe.aem \
 -D archetypeArtifactId=aem-project-archetype \
 -D archetypeVersion=26 \
 -D aemVersion="6.5.5" \
 -D appTitle="AEM655 Sandbox" \
 -D appId="app" \
 -D groupId="com.suayan"